﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        #region[Properties]
        int clientDetails { get; set; }
        double Price { get; set; }
        double Change { get; set; }
        #endregion
        static void Main(string[] args)
        {
            Client client = new Client();
            Program pr = new Program();
            Pizza pizza = new Pizza();
        a: Console.WriteLine("Press 1 to enter client details \nPress 2 to goto Pizza Menu");
            pr.clientDetails = Convert.ToInt32(Console.ReadLine());
            if (pr.clientDetails == 1)
            {
                client.ClientDetail();
                Console.WriteLine("Pizza Menu");
                pizza.ListofPizza();
                Console.WriteLine("Your Order is Ready! Total Price =$" + pizza.Sum);
                Console.Write("Amount given by Client :$");
                pr.Price = Convert.ToDouble(Console.ReadLine());
                pr.Change = pr.Price - pizza.Sum;
                Console.WriteLine("Change given to Client :$" + pr.Change);
                Console.WriteLine("**********************************************************");
                Console.WriteLine("******************THANK YOU FOR YOUR ORDER****************");
                Console.WriteLine("**********************************************************");
            }
            else if (pr.clientDetails == 2)
            {
                Console.WriteLine("Pizza Menu");
                pizza.ListofPizza();
                if (client.Name != null)
                {
                    Console.WriteLine("Your Order is Ready! Total Price =$" + pizza.Sum);
                    Console.Write("Amount given by Client :");
                    pr.Price = Convert.ToDouble(Console.ReadLine());
                    pr.Change = pr.Price - pizza.Sum;
                    Console.WriteLine("Change given to Client :" + pr.Change);
                }
                else
                {
                    client.ClientDetail();
                    Console.WriteLine("Your Order is Ready! Total Price =$" + pizza.Sum);
                    Console.Write("Amount given by Client :$");
                    pr.Price = Convert.ToDouble(Console.ReadLine());
                    pr.Change = pr.Price - pizza.Sum;
                    Console.WriteLine("Change given to Client :$" + pr.Change);
                }
                Console.WriteLine("**********************************************************");
                Console.WriteLine("******************THANK YOU FOR YOUR ORDER****************");
                Console.WriteLine("**********************************************************");
            }
            else
            {
                Console.WriteLine("Please choose the correct option");
                goto a;
            }

            Console.ReadLine();

        }
    }
    class Client
    {
        #region[Properties]
        public string Name { get; set; }
        string Address { get; set; }
        Int64 PhoneNumber { get; set; }
        #endregion

        #region[Methods]
        public void ClientDetail()
        {
            Console.Write("Enter Client Name : ");
            Name = Convert.ToString(Console.ReadLine());
            Console.Write("Enter Client Address : ");
            Address = Convert.ToString(Console.ReadLine());
            Console.Write("Enter Client Contact : ");
            PhoneNumber = Convert.ToInt64(Console.ReadLine());
        }
        #endregion
    }
}

class Pizza
{
    #region[Properties]
    int size { get; set; }
    int ChoiceofPizza { get; set; }
    int ChoiceofDrink { get; set; }
    double SmallsizePrice = 10.00;
    double MediumsizePrice = 15.00;
    double LargesizePrice = 20.00;
    public double Sum = 0.00;
    double[] Cost = new double[100];
    int NumberofItems = 0;
    double applejuice = 11.00;
    double orangejuice = 15.00;
    double pineapplejuice = 20.00;
    double colddrink = 30.00;
    #endregion

    #region[Methods]
    public void ListofPizza()
    {
        string[] plist = { "Margherita Pizza", "Double Cheese Pizza", "Tandoori Paneer Pizza", "Country Fest Pizza" };
        int i = 0;
        while (i < plist.Length)
        {
            Console.WriteLine((i + 1) + ". " + plist[i]);
            i++;
        }
    b: Console.WriteLine("Choose Any One By Type its Number");
        ChoiceofPizza = Convert.ToInt32(Console.ReadLine());
        if (ChoiceofPizza == 1)
        {
        u: Console.WriteLine("Select Size of Pizza Available :\n 1. Small : $10.00\n 2. Medium : $15.00\n 3. Large : $20.00");
            size = Convert.ToInt32(Console.ReadLine());
            Cost[0] = 0.00;
            if (size == 1)
            {
                Cost[0] = SmallsizePrice;
            }
            else if (size == 2)
            {
                Cost[0] = MediumsizePrice;
            }
            else if (size == 3)
            {
                Cost[0] = LargesizePrice;
            }
            else
            {
                Console.WriteLine("Choose Correct Size");
                goto u;
            }
            Console.Write("Enter number of Items");
            NumberofItems = Convert.ToInt32(Console.ReadLine());
            Sum = Sum + (Cost[0] * NumberofItems);
        }
        else if (ChoiceofPizza == 2)
        {
        y: Console.WriteLine("Select Size of Pizza Available :\n 1. Small : $10.00\n 2. Medium : $15.00\n 3. Large : $20.00");
            size = Convert.ToInt32(Console.ReadLine());
            Cost[1] = 0.00;
            if (size == 1)
            {
                Cost[1] = SmallsizePrice;
            }
            else if (size == 2)
            {
                Cost[1] = MediumsizePrice;
            }
            else if (size == 3)
            {
                Cost[1] = LargesizePrice;
            }
            else
            {
                Console.WriteLine("Choose Correct Size");
                goto y;
            }
            Console.Write("Enter number of Items");
            NumberofItems = Convert.ToInt32(Console.ReadLine());
            Sum = Sum + (Cost[1] * NumberofItems);
        }
        else if (ChoiceofPizza == 3)
        {
        t: Console.WriteLine("Select Size of Pizza Available :\n 1. Small : $10.00\n 2. Medium : $15.00\n 3. Large : $20.00");
            size = Convert.ToInt32(Console.ReadLine());
            Cost[2] = 0.00;
            if (size == 1)
            {
                Cost[2] = SmallsizePrice;
            }
            else if (size == 2)
            {
                Cost[2] = MediumsizePrice;
            }
            else if (size == 3)
            {
                Cost[2] = LargesizePrice;
            }
            else
            {
                Console.WriteLine("Choose Correct Size");
                goto t;
            }
            Console.Write("Enter number of Items");
            NumberofItems = Convert.ToInt32(Console.ReadLine());
            Sum = Sum + (Cost[2] * NumberofItems);
        }
        else if (ChoiceofPizza == 4)
        {
        r: Console.WriteLine("Select Size of Pizza Available :\n 1. Small : $10.00\n 2. Medium : $15.00\n 3. Large : $20.00");
            size = Convert.ToInt32(Console.ReadLine());
            Cost[3] = 0.00;
            if (size == 1)
            {
                Cost[3] = SmallsizePrice;
            }
            else if (size == 2)
            {
                Cost[3] = MediumsizePrice;
            }
            else if (size == 3)
            {
                Cost[3] = LargesizePrice;
            }
            else
            {
                Console.WriteLine("Choose Correct Size");
                goto r;
            }
            Console.Write("Enter number of Items");
            NumberofItems = Convert.ToInt32(Console.ReadLine());
            Sum = Sum + (Cost[3] * NumberofItems);
        }
        else
        {
            Console.WriteLine("Select Atleast Any One");
            goto b;
        }
    p: Console.Write("You want to select another pizza Y: Yes/ N: No");
        string a = Convert.ToString(Console.ReadLine());
        if (a == "y" || a == "Y")
        {
            goto b;
        }
        else if (a == "n" || a == "N")
        {
        l: Console.Write("You want to select drink? Y/N");
            string choice = Convert.ToString(Console.ReadLine());
            if (choice == "Y" || choice == "y")
            {
                Console.WriteLine("Select Any Drink you want");
                string[] drinkslist = { "Apple Juice : $11.00", "orangejuice : $15.00", "pineapplejuice : $20.00", "colddrink : $30.00" };
                int j = 0;
                while (j < drinkslist.Length)
                {
                    Console.WriteLine((j + 1) + ". " + drinkslist[j]);
                    j++;
                }
            c: Console.Write("Select Any one by type its number: ");
                ChoiceofDrink = Convert.ToInt32(Console.ReadLine());
                if (ChoiceofDrink == 1)
                {
                    Cost[7] = applejuice;
                    Console.Write("Enter number of Items");
                    NumberofItems = Convert.ToInt32(Console.ReadLine());
                    Sum = Sum + (Cost[4] * NumberofItems);
                }
                else if (ChoiceofDrink == 2)
                {
                    Cost[8] = orangejuice;
                    Console.Write("Enter number of Items");
                    NumberofItems = Convert.ToInt32(Console.ReadLine());
                    Sum = Sum + (Cost[5] * NumberofItems);
                }
                else if (ChoiceofDrink == 3)
                {
                    Cost[9] = pineapplejuice;
                    Console.Write("Enter number of Items");
                    NumberofItems = Convert.ToInt32(Console.ReadLine());
                    Sum = Sum + (Cost[6] * NumberofItems);
                }
                else if (ChoiceofDrink == 4)
                {
                    Cost[10] = colddrink;
                    Console.Write("Enter number of Items");
                    NumberofItems = Convert.ToInt32(Console.ReadLine());
                    Sum = Sum + (Cost[7] * NumberofItems);
                }
                else
                {
                    Console.WriteLine("Please Select Atleast one");
                    goto c;
                }
            m: Console.Write("You want to select another drink? Y/N");
                string drink = Convert.ToString(Console.ReadLine());
                if (drink == "Y" || drink == "y")
                {
                    goto c;
                }
                else if (drink == "N" || drink == "n")
                {
                    Console.WriteLine("Thank You");
                }
                else
                {
                    Console.WriteLine("Please Select any one choice");
                    goto m;
                }
            }
            else if (choice == "N" || choice == "n")
            {
                Console.WriteLine("Thank You");
            }
            else
            {
                Console.WriteLine("Please Select any one choice");
                goto l;
            }

        }
        else
        {
            Console.WriteLine("Please select atleast one option");
            goto p;
        }
    }
    #endregion
}
